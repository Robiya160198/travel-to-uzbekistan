import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_travel/models/recommended.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class About extends StatefulWidget {
  final oldindex;
  final index;
  final type;

  About(this.oldindex, this.index, this.type);

  @override
  _AbooutState createState() => _AbooutState();
}

class _AbooutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.type != 2
          ? ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(15),
                  height: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(widget.type == 0
                          ? recommended[widget.oldindex]
                              .imageUrl[widget.index]
                              .img
                          : widget.type == 3
                              ? widget.oldindex == 0
                                  ? beaches[widget.index].imgUrl
                                  : widget.oldindex == 1
                                      ? rivers[widget.index].imgUrl
                                      : widget.oldindex == 2
                                          ? mountains[widget.index].imgUrl
                                          : seas[widget.index].imgUrl
                              : tavsiyu[widget.index].img),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Text(
                    widget.type == 0
                        ? recommended[widget.oldindex]
                            .imageUrl[widget.index]
                            .name
                        : widget.type == 3
                            ? widget.oldindex == 0
                                ? beaches[widget.index].name
                                : widget.oldindex == 1
                                    ? rivers[widget.index].name
                                    : widget.oldindex == 2
                                        ? mountains[widget.index].name
                                        : seas[widget.index].name
                            : tavsiyu[widget.index].name,
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.w700),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: 15, right: 15, bottom: 50),
                  child: Text(
                    widget.type == 0
                        ? recommended[widget.oldindex]
                            .imageUrl[widget.index]
                            .title
                        : widget.type == 3
                            ? widget.oldindex == 0
                                ? beaches[widget.index].description
                                : widget.oldindex == 1
                                    ? rivers[widget.index].description
                                    : widget.oldindex == 2
                                        ? mountains[widget.index].description
                                        : seas[widget.index].description
                            : tavsiyu[widget.index].title,
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: Colors.grey),
                  ),
                ),
              ],
            )
          : ListView(
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                Container(
                  height: 600,
                  margin: EdgeInsets.all(15),
                  child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 15),
                    child: StaggeredGridView.countBuilder(
                      crossAxisCount: 4,
                      itemCount: recommended[widget.oldindex]
                          .hotels[widget.index]
                          .images
                          .length,
                      itemBuilder: (BuildContext context, int index) =>
                          new Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(recommended[widget.oldindex]
                                .hotels[widget.index]
                                .images[index]),
                          ),
                        ),
                      ),
                      staggeredTileBuilder: (int index) {
                        return index % 3 == 0 ||
                                (recommended[widget.oldindex]
                                                .hotels[widget.index]
                                                .images
                                                .length -
                                            1 ==
                                        index &&
                                    recommended[widget.oldindex]
                                                .hotels[widget.index]
                                                .images
                                                .length %
                                            3 !=
                                        0)
                            ? StaggeredTile.count(4, 3)
                            : StaggeredTile.count(2, 2);
                      },
                      mainAxisSpacing: 4.0,
                      crossAxisSpacing: 4.0,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 15, bottom: 5),
                      child: Text(
                        recommended[widget.oldindex].hotels[widget.index].name,
                        style: TextStyle(
                            fontSize: 35, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                      child: Text(
                        recommended[widget.oldindex]
                            .hotels[widget.index]
                            .adress,
                        maxLines: 2,
                        style: TextStyle(
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: Colors.grey),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 15),
                      color: Colors.blue,
                      height: 3,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 15, right: 15, top: 15, bottom: 5),
                      child: Text(
                        recommended[widget.oldindex].hotels[widget.index].price,
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15, right: 15, bottom: 50),
                      child: Text(
                        'Bir kechaga',
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                    ),
                  ],
                ),
              ],
            ),
    );
  }
}
