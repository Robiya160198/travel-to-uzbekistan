import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_travel/models/recommended.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'about.dart';

class SelectedImage extends StatefulWidget {
  final indexOld;
  final type;

  SelectedImage(this.indexOld, this.type);

  @override
  _SelectedImageState createState() => _SelectedImageState();
}

class _SelectedImageState extends State<SelectedImage> {
  PageController pageController = PageController();
  int selected = 0;
  GlobalKey _key = GlobalKey();
  void pageChanged(int index) {
    setState(() {
      selected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: Stack(
      children: <Widget>[
        PageView(
          physics: BouncingScrollPhysics(),
          controller: pageController,
          scrollDirection: Axis.horizontal,
          children: List.generate(
            widget.type == 0
                ? recommended[widget.indexOld].imageUrl.length
                : recommended[widget.indexOld].hotels.length,
            (int index) => Container(
              width: 333,
              height: 218,
              decoration: BoxDecoration(
                  image: DecorationImage(
                fit: BoxFit.cover,
                image: CachedNetworkImageProvider(
                  widget.type == 0
                      ? recommended[widget.indexOld].imageUrl[index].img
                      : recommended[widget.indexOld].hotels[index].images[0],
                ),
              )),
            ),
          ),
          onPageChanged: (value) => {pageChanged(value)},
        ),
        Container(
          height: 57.6,
          margin: EdgeInsets.fromLTRB(28.8, 25, 28.8, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 57.6,
                  height: 57.6,
                  padding:
                      EdgeInsets.only(right: 22, bottom: 22, left: 16, top: 16),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(9.6)),
                      color: Color(0x080a0928)),
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                ),
              ),
//              Container(
//                width: 57.6,
//                height: 57.6,
//                padding:
//                    EdgeInsets.only(right: 22, bottom: 22, left: 18, top: 18),
//                decoration: BoxDecoration(
//                    borderRadius: BorderRadius.all(Radius.circular(9.6)),
//                    color: Color(0x080a0928)),
//                child: Icon(
//                  Icons.more,
//                  color: Colors.white,
//                ),
//              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomLeft,
          child: Container(
              margin:
                  EdgeInsets.only(left: 28.8, bottom: 40, right: 28.8, top: 70),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 19.2),
                      child: Stack(
                        children: <Widget>[
                          ClipRect(
                            child: BackdropFilter(
                              filter: ImageFilter.blur(sigmaY: 3, sigmaX: 3),
                              child: Text(
                                recommended[widget.indexOld].name,
                                style: TextStyle(
                                    fontSize: 36,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 400, left: 120),
                      child: SmoothPageIndicator(
                        controller: pageController,
                        count: widget.type == 0
                            ? recommended[widget.indexOld].imageUrl.length
                            : recommended[widget.indexOld].hotels.length,
                        effect: ExpandingDotsEffect(
                          activeDotColor: Color(0xffffffff),
                          dotColor: Color(0xffdedede),
                          dotHeight: 4.8,
                          dotWidth: 6,
                          spacing: 4.8,
                        ),
                      ),
                    ),
                  ])),
        ),
        IndexedStack(
          children: List.generate(
            5,
            (index) => Visibility(
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  margin: EdgeInsets.only(top: 100),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            EdgeInsets.only(top: 40, left: 28.8, right: 28.80),
                        child: Stack(
                          children: <Widget>[
                            ClipRect(
                              child: BackdropFilter(
                                filter: ImageFilter.blur(sigmaY: 3, sigmaX: 3),
                                child: Text(
                                  widget.type == 0
                                      ? recommended[widget.indexOld]
                                          .imageUrl[index]
                                          .name
                                      : recommended[widget.indexOld]
                                          .hotels[index]
                                          .name,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      fontSize: 36,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 300, right: 20),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: Container(
                            width: 125,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 12, horizontal: 18),
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => About(
                                              widget.indexOld,
                                              index,
                                              widget.type)));
                                },
                                child: Row(
                                  children: <Widget>[
                                    Text(
                                      widget.type == 0 ? "haqida" : "ko'proq",
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    Icon(
                                      Icons.navigate_next,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              maintainState: true,
              visible: selected == index,
            ),
          ),
          index: selected,
        ),
      ],
    )));
  }
}
