import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_travel/models/recommended.dart';
import 'package:flutter_app_travel/selectedimage.dart';
import 'package:flutter_app_travel/test.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'about.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  final PageController pageController = PageController(viewportFraction: 0.877);
  TabController tabController;
  TabController tabController1;
  int selectedIndex = 0;
  int selectedIndex1 = 0;
  Category current = Category(Color(0xffFFe8ec), Colors.pink, 'Sanatoriyalar',
      "assets/svg/beach.svg", beaches);

  @override
  void initState() {
    tabController =
        TabController(initialIndex: selectedIndex, length: 4, vsync: this);
    tabController1 =
        TabController(initialIndex: selectedIndex1, length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: ListView(
            physics: BouncingScrollPhysics(),
            children: <Widget>[
              Container(
                height: 57.6,
                margin: EdgeInsets.fromLTRB(28.8, 25, 28.8, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: 57.6,
                      height: 57.6,
                      padding: EdgeInsets.all(18),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(9.6)),
                          color: Color(0x080a0928)),
                      child: Icon(Icons.menu),
                    ),
                    Container(
                      width: 57.6,
                      height: 57.6,
                      padding: EdgeInsets.all(18),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(9.6)),
                          color: Color(0x080a0928)),
                      child: Icon(Icons.search),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30, left: 28.8, right: 28.8),
                child: Text(
                  "O'zbekiston bo'ylab\nsayohat qiling",
                  style: TextStyle(fontSize: 36, fontWeight: FontWeight.w700),
                ),
              ),
              Container(
                height: 30,
                margin: EdgeInsets.only(top: 28, left: 10),
                child: DefaultTabController(
                  length: 3,
                  child: TabBar(
                      controller: tabController1,
                      labelPadding: EdgeInsets.only(left: 14.4, right: 14.4),
                      indicatorPadding:
                          EdgeInsets.only(left: 14.4, right: 14.4),
                      isScrollable: true,
                      labelColor: Colors.black,
                      unselectedLabelColor: Color(0xff8a8a8a),
                      labelStyle:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w700),
                      tabs: <Widget>[
                        Tab(
                          child: Container(
                            child: Text('Tavsiya qilingan'),
                          ),
                        ),
                        Tab(
                          child: Container(
                            child: Text('Mashhur joylar'),
                          ),
                        ),
                        Tab(
                          child: Container(
                            child: Text('Mehmonxonalar'),
                          ),
                        ),
                      ],
                      onTap: (int index) {
                        setState(() {
                          selectedIndex1 = index;
                          tabController1.animateTo(index);
                        });
                      }),
                ),
              ),
              IndexedStack(
                children: [
                  Visibility(
                    child: TypeBuild(recommended, 0),
                    maintainState: true,
                    visible: selectedIndex1 == 0,
                  ),
                  Visibility(
                    child: TypeBuild(tavsiyu, 1),
                    maintainState: true,
                    visible: selectedIndex1 == 1,
                  ),
                  Visibility(
                    child: TypeBuild(recommended, 2),
                    maintainState: true,
                    visible: selectedIndex1 == 2,
                  ),
                ],
                index: selectedIndex1,
              ),
              Padding(
                padding: EdgeInsets.only(left: 28.8, top: 25),
                child: SmoothPageIndicator(
                  controller: pageController,
                  count: recommended.length,
                  effect: ExpandingDotsEffect(
                      activeDotColor: Color(0xff8a8a8a),
                      dotColor: Color(0xffababab),
                      dotHeight: 4.8,
                      dotWidth: 6,
                      spacing: 4.8),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30, left: 28.8, right: 28.8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Eng so\'lim\njoylar',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 28,
                          fontWeight: FontWeight.w700),
                    ),
                    Text(
                      "Barchasi",
                      style: TextStyle(
                          color: Color(0xff8a8a8a),
                          fontSize: 16.8,
                          fontWeight: FontWeight.w700),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 25),
                height: 45,
                child: TabBar(
                    labelPadding: EdgeInsets.symmetric(horizontal: 3),
                    controller: tabController,
                    isScrollable: true,
                    indicatorColor: Color(0xfff7fff7),
                    tabs: categories.map((Category category) {
                      return Tab(
                        child: Container(
                          margin: EdgeInsets.only(left: 25.8),
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(9.6)),
                              color: category.backColor),
                          child: Padding(
                            padding: EdgeInsets.only(right: 25, left: 25),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                SvgPicture.asset(
                                  category.img,
                                  height: 16,
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  category.name,
                                  style: TextStyle(
                                      color: category.textColor,
                                      fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }).toList(),
                    onTap: (int index) {
                      setState(() {
                        selectedIndex = index;
                        tabController.animateTo(index);
                      });
                    }),
              ),
              IndexedStack(
                children: [
                  Visibility(
                    child: CategoryBuild(categories[0].list, 0),
                    maintainState: true,
                    visible: selectedIndex == 0,
                  ),
                  Visibility(
                    child: CategoryBuild(categories[1].list, 1),
                    maintainState: true,
                    visible: selectedIndex == 1,
                  ),
                  Visibility(
                    child: CategoryBuild(categories[2].list, 2),
                    maintainState: true,
                    visible: selectedIndex == 2,
                  ),
                  Visibility(
                    child: CategoryBuild(categories[3].list, 3),
                    maintainState: true,
                    visible: selectedIndex == 3,
                  ),
                ],
                index: selectedIndex,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget CategoryBuild(List list, int oldindex) {
    return Container(
      margin: EdgeInsets.only(top: 28.8, bottom: 30),
      height: 550,
      child: ListView.builder(
          itemCount: list.length,
          padding: EdgeInsets.only(right: 12, left: 28.8, bottom: 100),
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => About(oldindex, index, 3)));
              },
              child: Container(
                height: 180,
                margin: EdgeInsets.only(right: 16.8, bottom: 20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(9.6)),
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(list[index].imgUrl))),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Test(list[index])));
                  },
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        bottom: 10,
                        left: 10,
                        child: ClipRect(
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaY: 2, sigmaX: 2),
                            child: Container(
                              height: 36,
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              alignment: Alignment.center,
                              child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 6,
                                  ),
                                  Text(
                                    list[index].name,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }

  Widget TypeBuild(List list, int type) {
    return Container(
      height: 218,
      margin: EdgeInsets.only(top: 16),
      child: ListView.builder(
        itemCount: list.length,
        physics: BouncingScrollPhysics(),
        controller: pageController,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => type != 1
                          ? SelectedImage(index, type)
                          : About(0, index, 1)));
            },
            child: Container(
              margin:
                  EdgeInsets.only(right: 28.8, left: index == 0 ? 28.8 : 0.0),
              width: 300,
              height: 218,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(type == 1
                          ? list[index].img
                          : list[index].imageUrl[0].img))),
              child: GestureDetector(
                onTap: () {
                  if (type != 1)
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => Test(list[index])));
                },
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      bottom: 14,
                      left: 14,
                      child: ClipRect(
                        child: BackdropFilter(
                          filter: ImageFilter.blur(sigmaY: 2, sigmaX: 2),
                          child: Container(
                            height: 36,
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            alignment: Alignment.center,
                            child: Row(
                              children: <Widget>[
                                if (type != 1)
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.white,
                                  ),
                                SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  list[index].name,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
