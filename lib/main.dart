import 'package:flutter/material.dart';
import 'package:flutter_app_travel/selectedimage.dart';
import 'package:flutter_app_travel/test.dart';

import 'homescreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Color(0xfff7fff7),
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Raleway"),
      home: HomeScreen(),
    );
  }
}
