import 'package:flutter/material.dart';

class Recommended {
  final String name;
  final double latitude;
  final double longitude;
  final List<Model> imageUrl;
  final List<Hotel> hotels;

  Recommended(
      this.name, this.latitude, this.longitude, this.imageUrl, this.hotels);
}

class Model {
  String img;
  String name;
  String title;

  Model(this.img, this.name, this.title);
}

class Hotel {
  final List<String> images;
  final String name;
  final String price;
  final String adress;

  Hotel(this.images, this.name, this.price, this.adress);
}

List<Model> tavsiyu = [
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-15-6f85fef943abb58560363b88ca0b7f13.jpg",
      "Tuzkon ko'li",
      "Bu ko'l Jizzax viloyatida, Jizzaxdan 56 kilometr shimoli-sharqda, Qizilqum cho'lining sharqiy qismida joylashgan. Taxminlarga ko'ra, Tuzkon ko'li - Orol - Kaspiy davridagi suv havzasining bir qismidir. Ko'l suvi xech qayerga oqmaydi, shuningdek, u bir oz sho'rroq bo'lib, shimoli-g'arbda Aydarko'l bilan bog'lanadi. Bu ko'llarning umuiy maydoni - taxminan 3750 kilometr kvadrat. Bir yilda ko'llar tizimida 2000 tonnagacha baliq ovlanadi. "),
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-1-80b9493d9051da28375150dfb177932b.jpg",
      "Borsa Kelmas ko'li ",
      " Borsa Kelmas chuqir ko'li Ustyurt platosida joylashgan bo'lib, taxminan 30 million yil muqaddam Tetis qadimiy dengizining bir qismi bo'lgan. Xozirda dengiz o'rinida Qoraqum, Qizilqum, Ustyurt platosi va ushbi ko'l qolgan. Ko'lning nomi haqiqatga mos tushadi, unga yaqinlashish juda havfli bo'lib, ko'plab jarliklar va ingicha tuz qatlami bilan qoplangan daryoning muzlamay qolgan joylariga duch kelish mumkin.  Biroq mahalliy aholining so'zlariga ko'ra, agarda rezina etiklar kiyib, botqoqdan eson-omon o'tib olsa, ko'lning ustki qismigacha chiqish mumkin. U yer esa, huddi tosh kabi qattiq bo'lib, shuning uchun ko'l ustida sayr qilish mumkin bo'ladi. Hattoki, suvda ham yurish mumkin! Biroq, juda sho'r suvda. "),
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-3-80b9493d9051da28375150dfb177932b.jpg",
      "Platov daryosi vodiysi",
      "Platov so'zi temir tog' ma'nosini anglatadi. Qadimgi davrlardan vodiyda temir rudasi qazib olingan. Platov daryosi Toshkentdan, taxminan 100 kilometr masofada joylashgan bo'lib, u yerda tashrif etish uchun bir vaqtning o'zida bir necha joylar bor. Odatda, turistik yo'nalish 38 metrlik Platov sharsharasidan boshlanadi. Undan bir oz yuqorida qayinzor bor bo'lib, unda Qizil kitobga kiritilgan xayvonlarni uchratish mumkin. Platov kanyoni alohida e'tiborga loyiq bo'lib, ajoyib manzaralar yaratuvchi relyeflarga ega. Ular tabiat va vaqt qanchalar buyuk ekanini eslatib turadi. Eng katta taasurotlardan biri - bu Platov daryosi bo'ylab sayohatdir. U yerda poleolit davrida qadimiy odamlar yashagan bo'lib, Obi-Raxmat sun'iy g'orlari sifatida mashxur. Undan sal yuqorida yana bir ajoyib joy - g'or bor. Bu yerlarga aprelda, lolalar ochilgan vaqtda kelish kerak. Lolalar orasida Kaufman lolasi, Greyg lolasi va boshqa turdagilarini uchratish mumkin. "),
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-7-6f85fef943abb58560363b88ca0b7f13.jpg",
      "Sitorai Mohi-hosa ",
      "Bu joy Buxoroning oxirgi amiri Said Olimxonning shahar tashqarisidagi qarorgohi bo'lgan. Qizil armiya 1920 yili Buxoroni bosib olguniga qadar Amir Olimxon uni boshqargan. Bu bino ikki rus muhandisi tomonidan loyixalashtirilgan bo'lib, uning bezak ishlari bilan o'sha vaqtning eng yaxshi usta-hunarmandlari shug'ullangan. Arxitektura uslublarining bunday noodatiy uyg'unligi boshqa xech qayerda uchramaydi. Xozirgi kunda qarorgoh binosida amaliy san'at muzeyi joylashgan. Biroq, asosiy eksponat, albatta, saroy binosining o'zidir. Aynan shu yerdan Buxoroning oxirgi amiri Afg'onistonga qochib ketishga majbur bo'lgan. "),
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-21-1e7916c91253b1404f2b31f5486f47e8.jpg",
      "Zangi ota maqbarasi",
      "Toshkent yaqinidagi shaharchada joylashgan bu maqbara Amir Temurning buyrug'iga ko'ra qurilgan. Zangi ota turk-islom olamidagi ulug' mutafakkir, hamda mutasavvif donishmandlaridan biri bo'lgan. Zangi ota Himmatiy laqabi bilan ham mashxur bo'lgan edi. U qora tanli, ushturlab ( tuya lab), baquvvat, barvasta gavdali odam bo'lgan. Bu yerda uning rafiqasi Anbar ona ham dafn etilgan. Maqbara majmuasi bir necha zonalardan iborat: katta bog' va XIV-XIX asrlarda qurilgan masjid, madrasa, minora va Anbar ona maqbarasi. 1914-1915 yillarda masjid oldida minora qurilgan. 2013 yilda esa, bog' va Namozgoh masjidini kengaytirish bo'yicha ishlar amalga oshirildi. "),
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-23-1e7916c91253b1404f2b31f5486f47e8.jpg",
      "Omonqo'ton g'ori",
      "Omonqo'ton karst g'ori Samarqand viloyatidagi Zarofshon tog'lari shimoliy yonbag'rilarida joylashgan. G'orni arxeolog David Lev 1947 yilda topgan. Bu yerdan siantrop insonning son suyagi aniqlangan. Omonqo'ton g'ori O'zbekistondagi eng qadimgi manzilgohlardan biridir. G'or chuqurligi 80 metr atrofida, u yerda bir-biri bilan kichik o'tish joylari orqali bog'langan o'ziga xos zallar bor. Omonqo'tonga yozning ikkinchi yarimida borgan yaxshiroq, u iyun o'rtalariga qadar suv bilan to'lgan holatda bo'ladi. "),
  Model(
      "https://odam.uz/upload/media/entries/2017-06/13/757-39-a19e9fec982ada470185938a57d9071b.jpg",
      "Qiriq qiz qal'a",
      " Qiriq qiz qal'a Xorazm hududida 1938 yilda o'tkazilgan arxeologik qazilma ishlari vaqtida, Beruniy shahridan 30 kilometr shimolroqda aniqlangan. Olimlarning fikrga ko'ra, qal'a eramizning I-VI asrlariga tegishli. Qazilma ishlari davomida zardo'shtiylar qabrlari topilgan. Afsonalardan biriga ko'ra, bu qal'ada Guloyim ismli malika yashahan bo'lib, u o'zining qiriq nafar safdoshi bilan chegarani mustaxkam saqlab, raqiblari hujumlaridan himoyalangan. Bugungi kungacha istehkom tuynuklari bo'lgan tashqi devorlar saqlanib qolgan."),
];
List<Model> buxoro = [
  Model(
      "https://d1bvpoagx8hqbg.cloudfront.net/originals/bukhara-uzbekistan-5b0b1386954561725e0ec083f7a33d98.jpg",
      "Ark",
      "Buxoro Arki shaharning eng qo’hna arxeologik yodgorligidir. Arkning old qismida Registon maydoni joylashgan. Ark devorlarining uzunligi 789,6 m. ni, hajmi esa 3.96 ga. ni tashkil qiladi. Afsonalarga ko’ra Arkning yatilishiga O’zbek epik qahramoni Siyovush asos solgan. Mashhur X asr. muarrixi Muxammad Narshaxiyning ma’lumotlariga ko’ra ark juda uzoq muqqad vayrona holda bo’lgan bo’lib, faqatgina VII asrda Buxor Xudot Bidun tomonidan qayta tiklangan. Arkning ichkarisida Chil Duxtaron masjidi saqlanib qolingan."),
  Model(
      "https://kalpak-travel.com/wp-content/uploads/2017/05/khoja-nasredin-bukhara-statue.jpg",
      "Xo'ja Nasriddin",
      "Buxoroning tarixiy markazida, mashhur Labi-Hovuz maydonida noyob yodgorlik bor. U qadimgi meʼmorchilikning betakror durdonalari orasida yorug‘lik uchquniga o‘xshaydi. Bu afsonaviy qahramon Xo‘ja Nasriddin Afandi haykalidir."),
  Model(
      "https://www.journalofnomads.com/wp-content/uploads/2019/10/Top-things-to-see-and-do-in-Bukhara-Uzbekistan.jpg",
      "Minorai Kalon",
      "(tojikcha: Минораи Калон — Katta minora), Arslonxon minorasi — Buxorodagi meʼmoriy yodgorlik. Poyi Kalon meʼmoriy ansamblining bir qismi. Eski Buxoroning eng baland inshooti, balandligi 46,5 metr."),
  Model(
      "https://live.staticflickr.com/3734/10517247685_b2e374711c_b.jpg",
      "Labi Hovuz",
      " Buxorodagi meʼmoriy yodgorlik (17-asr). Dastlab bozor maydoni boʻlgan. Maydon oʻrtasida katta hovuz qazilib (1620 yil) atroflari sinchlar bilan mustahkamlangan, harsanglardan zinapoyalar, marmardan tarnovlar ishlangan. Shagʻal va tuproq bilan toʻldirilib shibbalangan. Hovuz eni 36 m., boʻyi 45,5 m., chuq. 5 m. Uning gʻarbida Nodir Devonbegi xonaqohi, sharqida Nodir Devonbegi madrasasi, shim.da Koʻkaldosh madrasasi va Ernazar elchi madrasasi ( saqlanmagan) qad koʻtargan. Hozirgi hovuz atrofiga chinorlar ekilib, choyxona qurilgan. Yodgorliklar taʼmirlangan"),
  Model(
      "https://kalpak-travel.com/wp-content/uploads/2017/05/bukhara-samanid-mausoleum.jpg",
      "Ismoil Samoniy maqbarasi",
      "Buxoro shahar madaniyat va istirohat bogʻi hududida joylashgan koʻhna meʼmoriy yodgorlik, Somoniylar davlatining asoschisi Ismoil Somoniy va uning avlodlari maqbarasi (taxminan 864—868). Maqbara Oʻrta Osiyo meʼmorligi va sanʼati tarixidagi dastlabki maqbaralardan boʻlib, uning tuzilishida qadimiy sugd meʼmorligining anʼanalari saqlanib qolgan buyuk meʼmoriy asardir.")
];
List<Recommended> recommended = [
  Recommended("Buxoro", 39.7675529, 64.4231326, buxoro, [
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/1161_1583123098RJpY_1024x768.JPG",
      "https://mybooking.uz/uploads/hotel/images/1161_1583123236QWq7_1024x768.JPG",
      "https://mybooking.uz/uploads/hotel/images/1161_1583123131KlSS_1024x768.JPG",
      "https://mybooking.uz/uploads/hotel/images/1161_158312312355Fo_1024x768.JPG",
      "https://mybooking.uz/uploads/hotel/images/1161_1583123139KcEg_1024x768.JPG",
      "https://mybooking.uz/uploads/hotel/images/1161_1583123115aVjk_1024x768.JPG"
    ], "Ark", "855 000 UZS", "Abu Hafsi Kabir ko'chasi, Buxoro"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/996_1568977708DjFn_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/996_1568977708v0Sf_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_15676153116BgS_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/996_1568977714v0JB_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/996_1568977716KPp8_1024x768.jpg"
    ], "Zargaron Plaza Mehmonxonasi", "1 070 000 UZS",
        "B. Naqshband ko'chasi ,256, Buxoro"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/24_1574524725UfJ2_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/24_15745247343dA2_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/24_15745247354wrA_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/24_1574524727XrNg_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/24_15745247259jdx_1024x768.jpg"
    ], "Kukaldosh", "600 000 UZS", "M.Ambar ko`chasi 115, Buxoro"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/1023_1569054046Co27_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1023_1569054047CitS_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1023_1569054048gV5o_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1023_1569054049KGP4_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1023_1569054051Y5FM_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1023_1569054049YQKx_1024x768.jpg"
    ], "Minorai Kalon Mehmonxonasi", "980 000 UZS",
        "X. Ibodov ko'chasi, 11, Buxoro"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/1240_1584350740lWlq_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1240_1584350740wgZE_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1240_1584350739vvZE_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1240_1583821565QnP4_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1240_1584350741JlK9_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/1240_1584350744E3Lw_1024x768.jpg"
    ], "Sultan", "660 000 UZS", "B. Naqshband ko'chasi, 100-uy, Buxoro"),
  ]),
  Recommended(
    "Samarqand",
    39.6550017,
    66.9756954,
    [
      Model(
          "https://fergana.agency/siteapi/media/images/369f4000-0e4b-40cf-bf87-8b8a8a07a719.jpeg",
          "Registon ansambli",
          "Samarqandning Registon maydonida 3 Madrasa (Ulugʻbek madrasasi, Tillakori Madrasasi, Sherdor Madrasasi) dan iborat meʼmoriy majmua. Dastlab Ulugʻbek madrasasi (1417—1420 yy) bunyod etilib, keyinchalik qarshisiga — maydon sharqiga Ulugʻbek xonaqohi (1424 y), shim.ga Mirzoyi karvonsaroyi, jan.ga Alika Koʻkaldosh Juma masjidi (1430 y) kurdirgan, yonida esa yogʻochdan xotamkori uslubida masjidi Muqatta va Abusaid madrasasi qurilgan. 15-asrning 20—40y.larida Registon hashamatli meʼmoriy ansamblga aylangan. 17-asrda Samarqand hokimi Yalangtoʻsh Bahodir vayrona holatdagi Ulugʻbek xonaqohi oʻrniga Sherdor Madrasa (1619—1635/36)ni, Mirzoyi karvonsaroyi oʻrniga Tillakori madrasa masjidi (1646/47— 1659/ 60)ni qurdirgan. Registon ansambli oʻzining rang-barang koshinkori bezaklari; naqshinkori peshtoqlari, ulkan gumbazlari bilan Oʻrta Osiyo meʼmorchligining noyob yodgorligi hisoblanadi"),
      Model(
          "https://www.journalofnomads.com/wp-content/uploads/2019/10/Shah-i-Zinda-Samarkand-Uzbekistan-1024x768.jpg",
          "Shohi Zinda",
          "Samarqanddagi meʼmoriy yodgorlik (11—20-asrlar); Afrosiyob tepaligi jan.da joylashgan qabristondagi maqbaralardan hamda masjid, minora va madrasadan iborat ansambl. Ularning eng qadimiysi Qusam ibn Abbos maqbarasi boʻlib, xalq orasida Shohizinda (tirik shoh) nomi bilan mashhur. Ansambl bir-biri bilan yoʻlak orqali bogʻlangan 3 guruh binolardan iborat. Quyi guruhdagi inshootlar Ulugʻbek oʻgʻli nomidan qurdirgan Abdulaziz chortogʻi (1434—35), unga shim.dan Davlat qushbegi madrasasi (1812—13) tutash, uning qarshisidagi ayvonli masjid naqshlar bilan nafis bezatilgan. 40 bosqichli tik zinapoya oʻrtaligʻining chap tomonida ziyoratxona va goʻrxonadan iborat qoʻshgumbazli maqbara (15-asr) joylashgan."),
      Model(
          "https://www.journalofnomads.com/wp-content/uploads/2019/08/Registan-Samarkand-Uzbekistan.jpg",
          "Tilla qori madrasasi",
          "Samarqanddagi meʼmoriy yodgorlik. Registon ansamblida Ulugʻbek davrida bunyod etilgan Mirzoyi karvonsaroyi (15-asr) oʻrnida Samarqand hokimi Yalangtoʻshbiy Bahodir Madrasa va jome masjid qurdirgan (1641—46). Karvonsaroy asosi ustiga Madrasa (shim. sharqiy qismida), hujralar oʻrnida peshtoq gumbazli masjid (gʻarbida) joylashgan. Dastlab 'Yalangtoʻshbiy kichik madrasasi' deb nomlangan. Keyinchalik masjid bezagida boshqa bir obida qurilishiga yetadigan miqdorda oltin sarflangani uchun 'tillakori' (tilladan ishlov berilgan) deb yuritila boshlagan. Tilla Qori madrasasidan shahar jome masjvdi va Madrasa sifatida foydalanilgan. Shuning uchun masjidi (63x22 m) katta va serhashamligi bilan boshqa madrasalardan ajralib turadi. Madrasa (70x70 m) ga gʻarbiy peshtoq orqali kiriladi. Peshtoq chuqur ravoqli, 2 qanotining oldi ravoqli, 2 qavatli hujralar, burchaklarini teng hajmdagi guldastamezanalar egallagan. Masjid xonaqohi (10,8x10,8 m)ning poygumbazi baland, uzoqdan ko'zga tashlanib turadi. Uning gumbazi nihoyasiga yetkazilmagan. Xonaqoh toʻriga marmardan mehrob va zinapoyali minbar ishlangan. O'z davrida zarhal naqshlar bilan jozibador bezatilgan xonaqohning 2 yonini oldi ravoqli, gumbaz tomli ayvon (yo'lak)lar egallagan. Peshtoq ravog'idagi marmar taxtachada bezak ishlari 1659—60 yillarda bajarilganligi yozilgan."),
      Model(
          "https://lh3.googleusercontent.com/proxy/gTsp1YY0IPmBgnh7R5DWzADT8breGf4r9J0WPdrK_jkyhlqQOCYHn57RwJ2Y2kVuT2dhR7fhVgtjNXC-cjmZObEY46c",
          "Bibixonim maqbarasi",
          "Amir Temurning katta xotini Bibixonim (Saroymulk xonim) 'oʻz onasi sharafi'ga (Klavixo) 14-asr oxirida Samarqandda qurdirgan Madrasa. Sayyoh, olim A. Vamberi taʼkidlashicha, bu madrasada 1000 ga yaqin talaba oʻqigan. Bibixonim madrasasi Bibixonim jome mayejidinint kirish peshtogʻi qarshisida joylashgan. Lekin jome mayejididan oldin qurilgan. Saroy tarixchisi Gʻiyosiddin Alining 'Amir Temur 1399 yil 10 mayda madrasada toʻxtab oʻtganligi' haqidagi maʼlumoti shundan dalolat beradi. Tarixnavis Fasih Havofiy (1375— 1442) taʼkidlashicha, Amir Temur jome masjidi qurilishini kelib koʻrarkan, meʼmoriy majmuada mutanosiblik buzilganligi—Madrasa peshtogʻi va ayvoni jome mayejidga nisbatan balandroq va qiyaroq boʻlib qolganligi uchun dargʻazab boʻladi va meʼmorlar (Xoja Mahmud Dovud va Muhammad Jald)ni jazoladi. Arxeologik qazishlar ham shu nomutanosiblikni tasdiqlaydi. Bino peshtogʻi qanchalik mahobatli boʻlmasin, uning qarshisidagi binoga monand emasligi koʻzga tashlanib qolgan. Meʼmoriy uslub talabiga koʻra, nomaqbul tushgan Madrasa peshtogʻi buzib tashlashga mahkum boʻldi. Shuning uchun ham keyingi ayerlarda Madrasa qoldiqlari buzib tashlandi."),
      Model(
          "https://previews.123rf.com/images/alexela82/alexela821904/alexela82190400109/120230016-samarkand-gur-e-amir-complex-mausoleum-illuminated-facade-with-muqarna-honeycomb-and-tomb-of-amir-te.jpg",
          "Go'ri Amir maqbarsi",
          "(14-asr oxiri — 1405 y.) — Samarqanddagi meʼmoriy yodgorlik. Xalq orasida Go‘ri Amir yoki Go‘ri Mir (Mir Sayyid Baraka) deb nomlanib kelinadi. Maqbaraga temuriylar sulolasiga mansub kishilar (Amir Temur, uning piri Mir Sayyid Baraka, o‘g‘illari Umar-shayx, Mironshoh va Shohrux, nabiralari Muhammad Sulton, Ulugʻbek va boshqalar) dafn etilgan. Boburning taʼkidlashicha, dastlab Temurning nabirasi Muhammad Sulton Mirzo Samarqand qalʼasi jan.da Toshqo‘rg‘on — chaqarda Madrasa qurdirgan. Muhammad Sulton halok bo‘lgach (1403), Amir Temur uning xotirasiga maqbara qurish haqida farmon berdi. Maqbara Madrasa hovlisining to‘riga bunyod etilgan.")
    ],
    [
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/897_1562732291TgLr_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_15626918391pJa_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1562691833W9Ax_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_15626918413Wrm_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1562691835kWjD_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1562691837FyAM_1024x768.jpg"
      ], "Emirxan Mehmonxonasi", "3 618 000 UZS",
          "Daxbet ko'chasi, 46А, Samarqand"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/_1568283707jBPE_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_15682837082H8n_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1568283707T6yV_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1568283710pZiV_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1568283704h9vV_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1568283705n625_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1568283703sc4z_1024x768.jpg"
      ], "Aleksandr Mehmonxonasi", "1 096 500 UZS",
          "Mirzo Ulug'bek ko'chasi, 62, Samarqand"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/_1568469256P18s_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1568469256uxFK_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1017_1568612909WVKw_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1017_1568612908zIR0_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1017_1568612912JFKu_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1017_15686129125C0x_1024x768.jpg"
      ], "Boulevard Palace", "860 000 UZS",
          "Orzu Maxmudov ko'chasi, 22, Samarqand"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/396_1573307150NmI5_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/396_15733070919pFF_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/396_1573307294faoV_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/396_1573307153Aemm_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/396_1573307107WmoG_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/396_1573307105eB3r_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/396_1573307137KFoQ_1024x768.jpg"
      ], "Royal Palace", "620 000 UZS", "Amir Temur ko'chasi 33, Samarqand"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/609_1573306873XKAB_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/609_1573306884UK9U_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/609_1573306864MUjK_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/609_1573306602uLNM_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/609_1573306473tX6y_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/609_1573306876iEWv_1024x768.jpg"
      ], "Margiana mehmonxonasi", "420 000 UZS",
          "Dagbitskaya ko`chasi, 91, Samarqand"),
    ],
  ),
  Recommended(
    "Xiva",
    41.3776893,
    60.3620006,
    [
      Model(
          "https://sonitesurfaces.com/wp-content/uploads/2020/01/kalta.jpg",
          "Ichan qal'a",
          "Oʻrta Osiyodagi yirik va noyob meʼmoriy yodgorlik. Xivaning ichki qalʼa (Shahriston) qismi. Ichan qalʼa shaharning Dishan qalʼa (tashqi qalʼa) qismidan kungurador devor bilan ajratilgan. U Xiva raboti (Dishan qalʼa)dan baland qoʻrgʻontepaga oʻxshab koʻrinadi. Ichan qalʼaga 4 darvoza (Bogʻcha dar-voza, Polvon darvoza, Tosh darvoza, Ota darvoza)dan kirilgan."),
      Model(
          "https://khivamuseum.uz/sites/default/files/kunya_ark3.jpg",
          "Ko'hna Ark saroyi",
          "Ko‘hna -Ark Xivaning qadimiy qal’asi Ichan-Qal’adagi xon saroylaridan biri. Bu erda 'Qadimgi Xorazm' bo‘limi ekspozitsiyasi tashkil qilingan. Umumiy maydoni 130x93m.'Qadimgi Xorazm' bo‘limi ko‘rgazmasi xonlar masjidida 'Qadimgi Xorazm' ko‘rgazmasi tashkil topdi. Ko‘rgazmada joylashgan eksponatlar soni 439 tashkil qiladi. Ular asosan Xorazm tarixining antik davridan Xiva xonligi tashkil topishigacha bo‘lgan tarixiy davrni o‘z ichiga oladi. Muzey 1920 yilda tashkil qilingan. “Qadimgi Xorazm” bo‘limida Xiva xonlarining taxti turgan zal va qabulxona ayvoni, o‘tov, 2 ta devonbegi ekspozitsiyalari, “Xorazm sopoli” ekspozitsiyasi, Xiva xonligi davrida tanga, pul ishlab chiqarilgan zarbxona, xonlarning yozgi va qishki masjidlari, qishki masjidda “Qadimgi Xorazm” ekspozitsiyasi joylashgan. Bo‘limda jami 984 ta eksponat bor. Bo‘limda eng qiziqarli eksponat - bu Xitoy kulollari tomonidan ishlangan Xitoyning Tanь imperiyasi davriga oid 700 yillik tarixga ega Xitoy chinni lagani.Bo‘limda qayta jihozlash ishlari amalga oshirilmoqda. Navbatdagi vazifalarimizdan biri O‘zbekiston Respublikasi mustaqilligining 24 yilligiga bag‘ishlab ko‘rgazmalar tashkil qilish, mustakillik bayramini munosib kutib olish, sayyohlarga namunali muzey xizmati ko‘rsatishdan iborat.Masjid qurilishi sanasi XIX asr.Ko‘rgazma joylashgan foydali maydoni 160kv.m"),
      Model(
          "https://media-cdn.tripadvisor.com/media/photo-m/1280/1a/ac/a9/0c/caption.jpg",
          "Ota darvoza",
          "Ichan qalʼaning gʻarbiy darvozasi. Olloqulixon hukmronligi davrida qurilgan (1828—29). Shermuhammad ota nomi bilan ham atalgan. Darvozaxona toʻgʻri toʻrtburchak tarhli (17,50×15,40 m), ichkarisi uzun dalon boʻlib, usti ikki katta gumbaz (diametri — 15,07 m) bilan krplangan. Da-lonning 2 tomonida bir-biriga oʻtiladigan oldi ochiq, gumbazli xonalar darvozabonlar xonasi vazifasini oʻtagan. Xonalarning biridan hujraga oʻtiladi. Hujradagi aylanma zinadan tomga chiqiladi. Darvoza peshtogʻi baland boʻlib, ikki yon tomonini guldastalar egallagan. Peshtoqining ravokli darchalari guldastalar qafasasi bilan bir balandlikda boʻlib, kunguralar bilan yakunlangan. Ota darvoza ning dastlabki koʻrinishi saqlanmagan, arxiv materiallari asosida qayta tiklangan (1974). 1978 yildan doʻkon uchun moslashtirilgan"),
      Model(
          "https://lh3.googleusercontent.com/proxy/8vLPIK-Y6QkVR4qAgYY_hvKbuMuFUWhsyggL7twxhQpkEy5p2QWFJ4sCXnrg6dcmJivLOrVsS3rMO49tTRXXd0dcgcFQ0--FIhXRw6_3pMeL3kgQJxNXgcE",
          "Islom xo'ja kompleksi",
          "Ushbu  madrasa va minora  XX asr boshida Vaziri Akbar Islom xo‘ja tomonidan  qurilgan.   Madrasa 42 ta hujradan iborat bo‘lib, unda 100 ta talaba o‘qigan. Uning old tomoni ikki qavatli bo‘lib, baland peshtoqi madrasani hashamatli qilib ko‘rsatib turibdi. Islom Xo‘ja madrasa   vaqfi uchun o‘z mulkidan 14451 tanob (5780 gektar) yer ajratgan.Islom xo‘ja madrasasi oldida qurilgan minoraning balandligi 51 metr bo‘lib, uning qurilishiga yarim million g‘isht ishlatilgan."),
      Model(
          "https://i.pinimg.com/564x/75/eb/b3/75ebb32d9f64a324b19091fcd1cc6610.jpg",
          "Kaltaminor",
          " Xivadagi meʼmoriy yodgorlik. Muhammad Aminxon madrasasining old tomonida joylashgan. Oʻrta Osiyoda eng katta va baland qilib qurish moʻljallangan (taxminan 70–80 m). 1852 y. qurila boshlanib 1855 y.da xonning vafoti tufayli chala qolgan (nomi shundan). Hozir K.ning bal. 26 m, asosining diametri — 14.2 m. Bezagida oq, yashil, feruza rangli koshinlar qoʻllanilgan (feruza rangli koshin koʻp ishlatilganidan koʻkminor deb ham ataladi)."),
    ],
    [
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/_1578163025cCAk_1024x768.JPG",
        "https://mybooking.uz/uploads/hotel/images/1169_15782933304FmX_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1169_1578293326YWrm_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1169_1578293328ZI4i_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_15781629747bHg_1024x768.JPG",
        "https://mybooking.uz/uploads/hotel/images/1169_15782933304FmX_1024x768.jpg"
      ], "Xiva Ibrohim", "230 000 UZS",
          "Pahlovon Maxmud ko'chasi, 30-uy, Хiva"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/_1567833258tJoD_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1567832916LKxy_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1567832976oc3r_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1567832976oc3r_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1000_1576840665b0Kx_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1567832839bJCa_1024x768.jpg"
      ], "New Star Xiva Mehmonxonasi", "350 000 UZS",
          "Anash Xalfa ko'chasi, 23, Хiva"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/_1576388562wdQH_1024x768.JPG",
        "https://mybooking.uz/uploads/hotel/images/_1576388595CWGm_1024x768.JPG",
        "https://mybooking.uz/uploads/hotel/images/_15763885893QIu_1024x768.JPG",
        "https://mybooking.uz/uploads/hotel/images/_1576388648iWBI_1024x768.JPG",
        "https://mybooking.uz/uploads/hotel/images/1130_1576657296plgv_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/_1576388563Oafw_1024x768.JPG"
      ], "Umar", "400 000 UZS", "El'obod ko'chasi, Хiva"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/1087_1572933509nyW8_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1087_1572933510gIA7_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1087_1572933509KFpz_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1087_1572933508Yarp_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1087_1572933510LyPV_1024x768.jpg"
      ], "Sayyoh Xiva House", "285 000 UZS", "Ichan qal'a, 105-uy, Хiva"),
      Hotel([
        "https://mybooking.uz/uploads/hotel/images/1180_1579846071ARS8_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1180_15798460734A4S_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1180_1579846072snSM_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1180_15798460710o14_1024x768.jpg",
        "https://mybooking.uz/uploads/hotel/images/1180_1579846074XlRK_1024x768.jpg"
      ], "Xiva Tosh Darvoza", "342 000 UZS",
          "Islom Xo'ja ko'chasi, 33-uy, Хiva"),
    ],
  ),
  Recommended("Toshkent", 41.3123363, 69.2787079, [
    Model(
        "https://i.pinimg.com/564x/df/fd/41/dffd410975506e27b0fb35c84e4aa8a8.jpg",
        "Teleminora",
        "Toshkent teleminorasi Ismail tomonidan qurilgan Oʻrta Osiyodagi eng baland binodir. Uning balandligi 375 metrdir. Rasmiy tilda Toshkent teleminorasi H-375m obyekti deb ataladi. Teleminora 1978-yildan boshlab, 6 yil davomida qurib bitilgan. 1985-yilning 15 yanvarida ishga tushirilgan.Ishga tushirilayotgan paytida Toshkent teleminorasi jahonda balandligi bo‘yicha to‘rtinchi o‘rinda turgan. O‘shanda u Toronto, Moskva, Nyu-York va Tokiodagi inshootlar qatorida dunyoning buyuk minoralari xalqaro federatsiyasining a’zosiga aylangan. Bugungi kunda u Markaziy Osiyoning shu xildagi yagona va eng baland binosi hisoblanadi. MDH davlatlari teleminoralari ichida esa, Ostankinodan keyingi ikkinchi o‘rinni egallaydi."),
    Model(
        "https://upload.wikimedia.org/wikipedia/commons/3/3d/%D0%9C%D0%B5%D0%B4%D1%80%D0%B5%D1%81%D0%B5_%D0%9A%D1%83%D0%BA%D0%B5%D0%BB%D1%8C%D0%B4%D0%B0%D1%88_%D0%B2_%D0%A2%D0%B0%D1%88%D0%BA%D0%B5%D0%BD%D1%82%D0%B5.jpg",
        "Ko'kaldosh madrasasi",
        "Toshkentdagi meʼmoriy yodgorlik. Shahar markazining Chorsu maydonida joylashgan. Qulbobo Koʻkaldosh qurdirgan (1551—75). Madrasa (umumiy oʻlchami 62,7 × 44,9 m) bosh tarzi jan.ga qaragan. Darvozadan kiraverishda chapda masjid, oʻngda darsxona joylashgan. Masjid va darsxonalarning tomi oʻzaro kesishgan ravoqlar ustiga oʻrnatilgan gumbazlardan iborat. Miyonsaroy 7 gumbazli. Chorsi hovlisi (38 × 26,9 m) keng , hujralar va ochiq ayvonlar bilan oʻralgan. Hovlining atrofidagi hujralar 38 ta boʻlib, darsxona va masjid bilan oʻzaro bogʻlangan. Madrasa dastlab 3 qavatli boʻlgan. Meʼmorlar madrasani bezashda, asosan, binoning old tomoniga eʼtibor berishgan. Peshtoqi bilan birga bal. 19,73 m boʻlib, sirkor parchin va girih naqshlar bilan bezatilgan. Bosh tarzining 2 burchagidagi guldastalar tepasi qafasa bilan yakunlangan."),
    Model(
        "https://appreal.org/wp-content/uploads/2017/11/26-05-2017-07-52_e1430c732b132aa72ec19b9390e84d11_1495817558.0607.jpg",
        "Xazrati Imom masjidi",
        "Hastimom — Toshkentdagi meʼmoriy yodgorlik (16—20-asrlar); majmua imom Abu Bakr Muhammad ibn Ali Ismoil ash-Qaffol Shoshiy qabri atrofida shakllangan. Qabr va uning atrofida vujudga kelgan qabriston va meʼmoriy yodgorlik majmuasi Hazrati imom nomi bilan ataladi. Dastlab Baroqxon madrasasi, keyinroq Qaffol Shoshiy maqbarasi, 16-asr oxirlarida Suyunchxoʻjaxon va nomaʼlum maqbara, 1579 yilda Abdullaxon II mablagʻiga qoʻsh uslubida Shayx bobohoji maqbarasi qurilgan (saqlan-magan). 16-asrda majmua tarkibida sayrgoh bogʻ barpo etilgan, ayvonlar qurilgan, hovuz atrofi koʻkalamzorlashtirilgan, xalq sayillari oʻtkazilgan. 19-asr oʻrtalarida Toshkent namozgohi (Tillashayx masjidi), Moʻyi Muborak madrasasi, Jome masjidi (saqlanmagan) bunyod etilgan. 20-asr boshlarida Tillashayx masjidi qayta qurilgan. Eshon Boboxon ham shu joyda dafn etilgan. Hazrati Imom majmuasim.da Oʻzbekiston musulmonlari idorasi joylashgan."),
    Model(
        "https://storage.kun.uz/source/1/6agrfqUj-ZPMeJlsPWUiwJMdyUIkLbch.jpg",
        "Mustaqillik maydoni",
        " Toshkentning markaziy maydoni. Poytaxt aholisining ommaviy tantanalari va boshqa tadbirlar oʻtkaziladigan joy. Oʻzbekiston, Navoiy va Sharof Rashidov koʻchalari oraligʻida, Anhor kanali sohilida joylashgan. Maydon perimetral tarzda qurilgan. 1917-yilgacha Sobor maydoni, 1917—66-yillarda Qizil maydon, 1966—91-yillar Lenin nomidagi maydon, Oʻzbekiston mustaqillikka erishgach, 1992 yildan Mustaqillik maydoni deb atala boshlagan. Mustaqillik maydoni hajmiy tuzilishi ja-nubdan shimolga choʻzilgan koʻpyoqli hududiy mujassamotni tashkil qiladi. Ansambl meʼmoriy uslub va koʻlami jihatidan yaxlit turli maʼmuriy binolardan iborat. Bir necha marta rekonstruksiya qilingan. Hukumat uyining dastlabki loyihasi (1931, meʼmorlar V. Arxangelskiy, A. Petlina va A. Sidorov) oʻrta asr mahobatli meʼmoriy shakl yoʻnalishida (serustunli, ravokli, mayda gumbazli) boʻlgan. Keyinchalik mumtoz va milliy meʼmoriy anʼanalar oʻzlashtirilgan holda order tizimi yoʻnalishi amalga oshirilgan (meʼmorlar A. Boboxonov, V. Volchak, S. Polupanov) va zamonaviylik ruhi berilib qayta ishlangan (1946, 1954—65), xoz. kunda bu binolar oʻrnida yangi bino barpo etilishi loyixalashtirilmoqda."),
    Model(
        "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Tashkent_City_Park_at_night_2019.jpg/1200px-Tashkent_City_Park_at_night_2019.jpg",
        "Toshkent City",
        " Toshkent shahrining markazida qurilayotgan zamonaviy shaharcha. Navoiy shoh koʻchasi, Islom Karimov koʻchasi, Furqat koʻchasi va Olmazor koʻchasi oraligʻidagi 80 gektar maydonda qurilmoqda. Yaqin orada Alisher Navoiy, Xalqlar doʻstligi, Paxtakor, Oʻzbekiston metro bekatlari joylashgan. Shaharchada ishbilarmonlik markazlari, ofislar, turar joylar, parklar, va boshqa ijtimoiy obyektlar joylashadi. Qurilish xorijiy va mahalliy sarmoyadorlar ajratgan pullar hisobiga qurilmoqda va bir qismi qurilib, foydalanishga topshirilgan")
  ], [
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/_1570872675TJt2_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1570872664bLYV_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_15708726661cTs_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1570872668tcPj_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1570872673XDGF_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_15708726704qQl_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_15708726704qQl_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1570872666F5h2_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1570872667wyrn_1024x768.jpg"
    ], "Wyndham Tashkent Mehmonxonasi", "1 511 500 UZS",
        "Amir Temur ko'chasi, 7/8, Toshkent"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/543_1592068808dLbR_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/543_1592068847qDqJ_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/543_1592068878708L_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/543_1592068938AerV_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/543_1592069007fTG3_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/543_1592069163wWJh_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/543_1592068835Wmx0_1024x768.jpg"
    ], "Hotel Uzbekistan", "2 250 000 UZS",
        "Maxtumquli ko`chasi, 45, Toshkent"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/830_15807256931ifn_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/830_1580725702R7Z0_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/830_1580725694AXJH_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/830_1580725698ZY80_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/830_1580725699sDEd_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/830_1580725704eF9j_1024x768.jpg"
    ], "The Royal Mezbon Hotel & SPA", "1 100 000 UZS",
        "Kichik Xalqa Yo`li, 41B, Toshkent"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/_1565692503yHp5_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1565692501w5qr_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_156569250167qe_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1565692503u5Kj_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1565692505XFSy_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1565692504IOoX_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/957_1565695576uUC6_1024x768.jpg"
    ], "Royal Residence Mehmonxonasi", "1 200 000 UZS",
        "Yunusobod tumani, Ц-5, 81а uy., Toshkent"),
    Hotel([
      "https://mybooking.uz/uploads/hotel/images/_1569660251uBJy_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1569660246gOEu_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_15696602463z15_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1569660250Tq4b_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1569660251ebjo_1024x768.jpg",
      "https://mybooking.uz/uploads/hotel/images/_1569660251UuvZ_1024x768.jpg"
    ], "Praga Mehmonxonasi", "900 000 UZS",
        "Obid Akromxo'jayev ko'chasi, 21-uy, Toshkent"),
  ]),
];

class Beach {
  final String name;
  final String imgUrl;
  final double latitude;
  final double longitude;
  final String description;

  Beach(
      this.name, this.imgUrl, this.latitude, this.longitude, this.description);
}

List<Beach> beaches = [
  Beach(
      "Chimyon",
      "https://i2.wp.com/mice-uzbekistan.uz/wp-content/uploads/2017/03/chimyon-7.jpg?ssl=1",
      40.252222,
      71.576389,
      "Bu yerga borish g‘oyasi faol hordiq tarafdorlariga mo‘ljallangan. Chimyon – O‘zbekistonning tog‘ turizmi markazi. Qishda bu yerda chang‘ichilarni uchratish mumkin, yozda esa - mahalliy cho‘qqini zabt etish uchun kelgan ko‘plab guruhlarni.Sayohat balandlikka harakatlanishdan iborat bo‘ladi. 3000 metr balandlikkacha yo‘nalish nisbatan oson bo‘ladi - aniq yo‘l, keng joy. Biroq so‘nggi 300 metr masofada yo‘lda qoyalar uchraydi, bu yerda gid ko‘rsatmalariga rioya qilish zarur. O‘rta tayyorgarlik ko‘rgan kishi uchun 3309 metr balandlikka ko‘tarilish 6-8 soat vaqtni oladi.Shahargacha bo‘lgan masofa: 80 kilometr"),
  Beach(
      "Chortoq",
      "https://www.chortoqsan.uz/images/003.jpg",
      41.0715751,
      71.8188795,
      "Chortoq sanatoriyasi Namangan shahridan 25 km masofada, Chortoqsoy daryosi yoqasida, Chotqol tog’ adirlarida, dengiz satxidan 650 metr balandlikda joylashgan balneologik va iqlimiy kurortdir.Yurak-qon tomir, ovqat xazm qilish va tayanch-harakat a’zolari, asab tizimi, ginekologik va teri kasalliklari bilan og’rigan bemorlar uchun shifobaxsh tabiiy omillari mavjud bo’lib, yod, bromli va boshqa ko’pgina minerallarga boy bo’lgan termal ma’danli suvlari ichish va vannalar berish uchun qo’llaniladi. Shifobaxsh balchiq applikatsiya va galvano-balchiq usulida beriladi.O’tkir miokard infarktini kechirgan bemorlarning sog’ligini tiklash uchun reabilitatsiya bo’limi faoliyat ko’rsatadi.Zamonaviy tashxislash va davolash asbob-uskunalari yordamida fizioterapiya, lazer bilan davolash, suv bilan davolash, davolash badantarbiyasi, uqalash, psixoterapiya, fitobar va parhez ovqatlar sizning sog’ligingizni tiklash uchun faoliyat ko’rsatadi.      Yotoq binolari barcha qulayliklarga ega bo’lgan bir va ikki xonalardan iborat. Madaniyat saroyi, kutubxona, turli sport maydonchalari, sartaroshxona, aloqa bo’limi sizning xizmatingizda. Turli xildagi sayohat va ekskursiyalar uyushtiriladi.Manzil: 'Chortoq sanatoriyasi' OAJ 161004Namangan viloyati, Chortoq tumani, A.Saydaliyev ko’chasi, 5-uy.Telefon: (0-36941) 26-444 (Chortoq)"),
  Beach(
      "Zomin",
      "https://bookatour.me/uploads/images/d15e315037b89fce412c2d437c61def5.jpg",
      39.9619506,
      68.3969972,
      " 'ZOMIN' sanatoriyasi – Jizzax viloyatidagi iqlimiy kurort, Turkiston tog’ tizmasining shimoliy etaklarida, dengiz sathidan 2000 metr balandlikda, qo’riqxona xududida joylashgan. Chiroyli tog’ manzarasi, musaffo havo, quyosh nurlarining mo’lligi, ultrabinafsha radiatsiyasining yuqoriligi iqlimiy profilaktik va terapevtik muolajalar o’tkazishga qulay sharoitlar yaratadi. Davolash uchun ko’rsatmalar: nafas olish a’zolari va nerv tizimi kasalliklari (funksional).Sanatoriyda 3 yoshdan 14 yoshgacha bo’lgan bolalarni ota-onalari bilan birga qabul qilish uchun 'Ona-bola' bo’limi tashkil qilingan. 'Zomin' sanatoriysi eng yangi davolash-tashxislash apparatlari bilan jihozlangan.Asosiy davolash omili tog’ iqlimidir. Bundan tashqari, suv bilan davolash (sun’iy marvaridli, yod-bromli, igna bargli vannalar, dushlar, suv havzasi), fizioterapevtik, davolash badan tarbiyasi, torrenkur, uqalash, sauna, hamda bronxial astmali bemorlarni davolash uchun galokameradan foydalaniladi. Yotoq binosi shinam, bir va ikki o’rinli palatalardan tashkil topgan. Shu binoda klub, kutubxona, bar, bolalar uchun attraksionlari bo’lgan o’yin xonalari, sport maydonchalari mavjud. Ekskursiyalar tashkil qilinadi.Manzil: 'Zomin sanatoriyasi' MCHJ 130800 Jizzax viloyati, Zomin tumaniTelefon: (0-37222) 100-10 (Zomin)"),
  Beach(
      "Kosonsoy",
      "https://i.mycdn.me/i?r=AyH4iRPQ2q0otWIFepML2LxRV5qL_dHpm8ywt4HKHuIaSA",
      41.2520563,
      71.5452352,
      "'KOSONSOY' sanatoriyasi – Namangan viloyati Chotqol tog’ tizmalari etagida, Kosonsoy daryosi bo’yida joylashgan, iqlimiy va balneologik sanatoriydir.Sanatoriy xarakat-tayanch, yurak-qon tomir, oshqozon-ichak, a’zolari va asab tizimi kasalliklarini davolash uchun mo’ljallangan.Asosiy davolash omillari: musaffo havoli tog iqlimi (havoda kislorod zichligi 26 foiz) va tarkibi sulfat-xlorid-gidrokarbonad-natriyli, harorati 27 C tabiiy ma`danli suvidir. U ichish yo’li bilan hamda mineral vannalar qabul qilish yo’li bilan bemorlarni davolashda ishlatiladi.Sanatoriyda bemorlar shifobaxsh balchiq, parafin-ozokerit, uqalash, fizioterapiya, davolash jismoniy tarbiyasi, fitoterapiya va parhez ovqatlanish usullari bilan davolanadilar. Tashxis qo’yishda va davolashda zamonaviy uskunalar, klinik bioximik laboratoriya, shuningdek kardiolog, nevropatolog, ginekolog, stomatolog va boshqa mutaxassislar faoliyat ko’rsatadilar.Yotoq xonalari shinam qilib bezatilgan, barcha qulayliklarga ega bo’lgan bir va ikki o’rinli xonalardan iborat. Oshxonada shirin, lazzatli parhez taomlar tayyorlanadi, 'vitaminli stol' tashkil etilgan. Dam oluvchilar uchun madaniyat saroyi, kutubxona va maishiy xizmat ko’rsatish xonalari mavjud. Ekskursiyalar tashkil etiladi.Manzil: 'Kosonsoy sanatoriyasi' MCHJ 160300 Namangan viloyati, Kosonsoy shahri, Xalqlar do’stligi ko’chasi 21-uy Telefon: (0-36965) 21-192 (Kosonsoy)"),
  Beach(
      "Sitorai mohi xosa",
      "https://lh3.googleusercontent.com/proxy/pk_dR7eFcblwzfrCmDrAuGolPwd-H5UHFUUcST9v5lj5JNAtyvpy1M_zrWZ6sH0AbWqGpLDuzqZSzUolUYSM1NoO3FTp1FNEcGliyHkOs3a3r2csgqkUXNn-TFx2MzsXjbN0TOMkT4tgeLWyTxiEanBkEmiSg1WO",
      39.8105355,
      64.4392235,
      " 'SITORAI MOXI XOSA' sanatoriyasi iqlimiy va balneologik kurort. U Zarafshon daryosi vohasida, dengiz sathidan 500 metr balandlikda, Buxoro shahrining sharqiy chekkasida Buxoro amirining sobiq qarorgohi hududidagi so’lim bir maskanda joylashgan Davolash omillaridan oqlimi va sulfat-xlorid natriyli madanli suvi hisoblanadi va ular buyrak, asab tizimi, xarakat va hazm a’zolari kasalliklarida qo’llaniladi. Sanatoriyda zamonaviy tashxislash va davolash xonalari, fizioterapiya, lazeroterapiya, balchiq bilan davolash, jizmoniy tarbiya, uqalash, fitobar va parhezli ovqatlantirish sog’ligingizni tiklash maqsadlarida xizmat qiladi. Yotoq binosi bir va ikki o’rinli shinam xonalardan iborat. Shinam oshxona, klub, sport maydonchalari, kutubxona, muzey, basseyn, sauna. Sizning xizmatingizda Buxoro shahrining tarixiy obidalariga ekskursiyalar tashkil qilinadi. Manzil: 'Sitorai Moxi Xosa sanatoriyasi' 200126 Buxoro shahri, Moxi Xosa dahasi, shifokorlar ko’chasi 1-uy Telefon: (0-365) 228-50-66, 228-50-26, 228-50-88 "),
  Beach(
      "Qashqadaryo sohili",
      "https://putevka.uz/wp-content/uploads/2019/04/qashqadaryo_sohili_17_croped.jpg",
      38.83998,
      65.7927948,
      "'QASHQADARYO SOHILI' sanatoriyasi Qarshi shahrida, Qashqadaryo vohasidagi so’lim joylardan biri bo’lgan Qashqadaryo daryosining soxilida, dengiz satxidan 600 metr balandlikda joylashgan, iqlimiy balneologik sihatgohdir. Musaffo havo surunkali buyrak, yurak, qon-tomir, tayanch-harakat a’zolari, asab, ovqat xazm qilish a’zolari va ginekologik kasalliklarni davolashda muhim omildir. Tashxis qo’yishda zamonaviy kompyuterli ultra tovush tekshiruvi, kompyuterli EKG, endoskopiya, klinik va bioximik laboratoriya, nevropatolog, gastroentrolog, kardiolog, stomatolog, ginekolog, okulist, urolog shifokorlari faoliyat ko’rsatadilar. Davolashda musaffo havo, quyosh vannalari, shifobaxsh balchiq, ozokerit-parafin, madanlivannalar, dushlar, basseyn, sauna, davolash jismoniy tarbiya, igna-refleksoterapiya, uqalash, psixoterapiya, yuqori va past kuchlanishli toklar, lazeroterapiya, duodenal zondlash va ichaklarni yuvosh, parhez ovqatlanish kabi usullardan foydalaniladi. 1100-1130 metr chuqurlikdan termal kam minerallashgan, tarkibida xlorid-sulfat kaltsiy natriyli organik moddalarga boy suv davolash jarayonida keng qo’llaniladi. Manzil: 'Qashqadaryo sohili sanatoriyasi' 180104Qarshi shahri, N.Mirzayev ko’chasi, 12-uy Telefon: (0-37522) 6-17-26 (Qarshi)")
];

class River {
  final String name;
  final String imgUrl;
  final double latitude;
  final double longitude;
  final String description;
  River(
      this.name, this.imgUrl, this.latitude, this.longitude, this.description);
}

List<River> rivers = [
  River(
      "Parkentsoy",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm6.jpg",
      41.2902578,
      69.6754711,
      "Bu Toshkentga eng yaqin joylashgan ajoyib tosh darali manzaraga ega ko‘rkam maskan. Bu yerda “Kumushkon” dam olish uyiga joylashib, mazmunli hordiq chiqarish mumkin. Buloq, soy, qoyalar – bularning hech biri sizni befarq qoldirmaydi. Oqim bo‘ylab yuqoriga harakatlanib, sharsharalar chekkasiga borib ajoyib manzaraning guvohi bo‘lishingiz mumkin.Shahargacha bo‘lgan masofa: 50 kilometr"),
  River(
      "Gulkamsoy",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm11.jpg",
      41.4976997,
      70.0581969,
      "Katta Chimyon atrofidagi hudud ko‘plab tasviriy zonalarga bo‘lingan. Agar siz tirband shahardan dam olishni ixtiyor qilgan bo‘lsangiz, Gulkamsoy daralari dam olish uchun yana bir maqbul joy hisoblanadi. Soy atrofidagi soya-salqin joylar, kichik suv to‘siqlarini yengib o‘tish, sharshara sizga unutilmas taassurotlar va ekstremal his-tuyg‘ularni taqdim etadi. Hudud qulay yozgi supalar bilan jihozlangan, bu yerda oilaviy hordiq chiqarish mumkin.Shahargacha bo‘lgan masofa: 110 kilometr"),
  River(
      "Zomin",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm18.jpg",
      39.9619506,
      68.3969972,
      "Zomin milliy bog‘ida, dengiz sathidan 2 ming metr balandlikda 'Zomin' davolash-sog‘lomlashtirish sanatoriysi joylashgan.O‘ta toza havo, tog‘ manzaralari, ignabargli o‘simliklar - bularning barchasi kattalar va bolalarga sog‘lom turmush tarzi uchun eng yaxshi sharoitlarni yaratadi.Sanatoriyda zamonaviy tibbiyot uskunalari mavjud, shuningdek diagnostika, EKG, ultratovush va stomatologiya bo‘limlari ishlab turibdi. Tashxisdan keyin mehmonlarga zarur terapiya va tegishli davolash protseduralari tayinlanadi.Bu yerda ikkita hovuz, sauna bor, shuningdek, massaj, loy bilan davolash, natriy xlorid tuzi, ingalyatsiya uskunalari bilan davolash va boshqa xizmatlar mavjud."),
  River(
      "Beldirsoy",
      "https://i.mycdn.me/i?r=AyH4iRPQ2q0otWIFepML2LxRV5qL_dHpm8ywt4HKHuIaSA",
      41.4976997,
      70.0581969,
      "Tasavvur qiling: yozgi quyosh taftida toblangan hamda musaffo tog‘ havosi bilan to‘yingan xushbo‘y maysalar... Oilaviy hordiq uchun bundan yaxshisi bormi?! Beldirsoy nomini olgan yaylov aynan sayyohlarbop maskandir. Archazorlarni kezib, suvning tovushiga quloq tutish albatta oldinda kutayotgan ish kunlariga yangi kuch-quvvat baxsh etadi.Shahargacha bo‘lgan masofa: 75 kilometr"),
  River(
      "Oqsoq ota",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm17.jpg",
      41.4976997,
      70.0581969,
      "Bu yerda O‘zbekiston Milliy universitetining yozgi oromgohi joylashgan. Oqsoq ota - 30 kilometrdan ziyod chuqurlikka qarab ketuvchi keng dara. Bu maskanda qoyalar, soya-salqin o‘rmonlar va qarag‘ayzorlarni uchratishingiz shubhasiz. Shaharning shovqinidan keyin bu go‘zal maskanda bo‘lishni kim ham orzu qilmaydi deysiz?! Ushbu maskan ham sokin, ham faol dam olish tarafdorlariga to‘g‘ri keladi.Shahargacha bo‘lgan masofa: 120 kilometr"),
  River(
      "Ispay darasi",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm2.jpg",
      41.4976997,
      70.0581969,
      "Ispaysoy Qorabuloq postidan o‘tganda joylashgan. Bu yerda Ispay daryosining suvida zavq olib cho‘milish mumkin. Mahalliy aholi mehmonxona uylariga joylashish, shuningdek sharsharagacha otda sayr qilishni taklif qiladi.Pskem tizmasidagi muzliklar va tog' yo'llarining ko'pligi bu hududni tog' turizmi uchun qulay qiladi.Shahargacha bo'lgan masofa: 130 kilometr")
];

class Mauntain {
  final String name;
  final String imgUrl;
  final double latitude;
  final double longitude;
  final String description;
  Mauntain(
      this.name, this.imgUrl, this.latitude, this.longitude, this.description);
}

List<Mauntain> mountains = [
  Mauntain(
      "Kumbel tog‘i",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm14.jpg",
      41.4976997,
      70.0581969,
      "O‘zbekiston tog‘lari - quyoshli o‘lkaning eng diqqatga sazovor joylaridan. Cho‘qqiga chiqish istagini bildirayotganlar soni tobora oshib bormoqda. Ortiqcha qiyinchilik tug‘dirmaydigan sayohatsevarlarni Beldirsoyga, Kumbel tog‘iga taklif qilish mumkin. Uning balandligi - 2350 metr.Yo‘lning bir qismini osma dorda kesib o‘tish mumkin. yuqori stansiyada tushib, yana 40 daqiqa yurish kerak bo‘ladi. Tog‘da meteorologik stansiya joylashgan. U yerga tushdan keyin quyoshning botishini tomosha qilishga chiqqan kishi go‘zal manzaraning guvohi bo‘ladi.Shahargacha bo‘lgan masofa: 75 kilometr"),
  Mauntain(
      "Oqsoq ota darasi",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm17.jpg",
      41.4976997,
      70.0581969,
      "Bu yerda O‘zbekiston Milliy universitetining yozgi oromgohi joylashgan. Oqsoq ota - 30 kilometrdan ziyod chuqurlikka qarab ketuvchi keng dara. Bu maskanda qoyalar, soya-salqin o‘rmonlar va qarag‘ayzorlarni uchratishingiz shubhasiz. Shaharning shovqinidan keyin bu go‘zal maskanda bo‘lishni kim ham orzu qilmaydi deysiz?! Ushbu maskan ham sokin, ham faol dam olish tarafdorlariga to‘g‘ri keladi.Shahargacha bo‘lgan masofa: 120 kilometr"),
  Mauntain(
      "Katta Chimyon cho‘qqisi",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm12.jpg",
      41.4976997,
      70.0581969,
      "Bu yerga borish g‘oyasi faol hordiq tarafdorlariga mo‘ljallangan. Chimyon – O‘zbekistonning tog‘ turizmi markazi. Qishda bu yerda chang‘ichilarni uchratish mumkin, yozda esa - mahalliy cho‘qqini zabt etish uchun kelgan ko‘plab guruhlarni.Sayohat balandlikka harakatlanishdan iborat bo‘ladi. 3000 metr balandlikkacha yo‘nalish nisbatan oson bo‘ladi - aniq yo‘l, keng joy. Biroq so‘nggi 300 metr masofada yo‘lda qoyalar uchraydi, bu yerda gid ko‘rsatmalariga rioya qilish zarur. O‘rta tayyorgarlik ko‘rgan kishi uchun 3309 metr balandlikka ko‘tarilish 6-8 soat vaqtni oladi.Shahargacha bo‘lgan masofa: 80 kilometr"),
  Mauntain(
      "So‘qoq",
      "http://storage.kun.uz/source/uploads/2017mrap/tshkdm8.jpg",
      41.4976997,
      70.0581969,
      "Ushbu maskan o‘z landshaftining noan'anaviyligi bilan ajralib turadi. Bu yerda paydo bo‘lgan yo‘lovchi o‘zini xuddi Rossiyadagidek tasavvur qilishi mumkin. Sershox qarag‘ayzor shaharda yetishmaydigan salqin havo bilan ta'minlaydi. Bu yerda sayyoh tanlashi mumkin: meteostansiyagacha borish yoki Shoxqo‘rg‘on tog‘iga ko‘tarilish. Shoxqo‘rg‘ondan tog‘ tizmalari, Zarkent va So‘qoq qishloqlarining ajib manzarasi kishiga o‘zgacha zavq bag‘ishlaydi. Tog‘ga yaxshi jismoniy tayyorgarlik bilan 1,5-2 soatda ko‘tarilish mumkin. Bundan tashqari, mahalliy kaboblar ham mehmonlarni befarq qoldirmaydi. Shahargacha bo‘lgan masofa: 60 kilometr"),
  Mauntain(
      "Nurota",
      "https://www.advantour.com/img/uzbekistan/nurata/nuratau-mountains1.jpg",
      40.559982,
      65.710509,
      "Nurota tog‘lari — bu nafaqat davomiy sayohat uchun, balki dam olish kunlari uchun monand bo‘lgan ajoyib joy hamdir. Nurota Samarqand, Buxoro, Toshkent kabi yirik shaharlardan uzoq emas va bu yerga avtomobilda bir necha soat ichida yetib kelish mumkin. Tabiat yodgorliklari bilan bir qatorda, mahalliy aholining turmush tarzi ham katta qiziqish uyg‘otishi aniq. Aytmoqchi, Nurota tog‘laridan uzoq bo‘lmagan yerda, Aydarko‘lda sayyohlar orasida juda mashhur bo‘lgan o‘tov qarorgohi mavjud. Shuningdek, mehmonxona uylarda yotib qolish imkoniyati bilan birga, bu uylarda turli xil gilam va boshqa buyumlarning yaratilish jarayonida ishtirok etish mumkin. Bahorda va kuzda, bahorning ilk kunlarida hamda yig‘im-terim mavsumida nurotaliklar bayramlar o‘tkazilib, ularda mahalliy hofiz va raqqosalar, shuningdek, ommaviy o‘yinlarni ham kuzatish mumkin. Eng ommabop o‘yin — bu “Ko‘p-Kari”, ya’ni “Ko‘pchilik insonlar ishi” deb nomlanadi. O‘yinning maqsadi, so‘yilgan echkini ilib, marraga birinchi bo‘lib yetib kelishdadir. Bu o‘yin ko‘plab tamoshabinlarni jalb qilish bilan birga, o‘yin ishtirokchilarida ham shijoat uyg‘otadi. Nurota tog‘lari — bu bir umrga xotirada muhrlanib qoluvchi yodgorliklar, tarix va taassurotlarning bir qator majmuasidir!"),
  Mauntain(
      "Chorvoq",
      "https://www.afisha.uz/ui/catalog/2012/01/0697838_b.jpeg",
      41.635144,
      69.940021,
      "O‘zbekistonda plyaj ta’tilini o‘ylab yurgan bo‘lsangiz Toshkent viloyatining Bo‘stonliq tumanida joylashgan Chorvoq suv omboriga e’tibor qaratishingiz kerak. Dam olish maskaniga poytaxtdan taksi, poyezd yoki avtobusda borish mumkin.Ulkan suv havzasi bo‘yidagi jihozlangan plyajlar dam olish joylari, soyabonlar, an’anaviy yog‘och karavotlar va kafelarni taklif etadi. Bu yerda dam oluvchilar skuterda, katamaranda yoki suv bananida uchishi mumkin, ekstremal sport turlari muxlislari paraplanda uchib, tevarak atrofni o‘rganishlari mumkin.Chorvoqning butun qirg‘oq bo‘yi bo‘ylab 100 km masofada dam olish maskanlari, bolalar lageri va xususiy uylar mavjud bo‘lib, ularni o‘zingiz ham, istalgan mahalliy sayyohlik agentliklari orqali ham band qilishingiz mumkin.Plyaj ta‘tilini siz bu yerda yangi kashfiyotlar bilan mujassamlashtirib – Bo‘stonliq tumanidagi eng qadimiy arxeologik obidalarni ko‘rish, qoyatosh rasmlari va ibtidoiy odamlarning manzilgohlarini ko‘rish, qo‘riqlanadigan hududning sirli daralari va g‘orlarida sayr qilishingiz mumkin.")
];

class Sea {
  final String name;
  final String imgUrl;
  final double latitude;
  final double longitude;
  final String description;
  Sea(this.name, this.imgUrl, this.latitude, this.longitude, this.description);
}

List<Sea> seas = [
  Sea(
      "Urungach nefrit ko‘llari",
      "http://storage.kun.uz/source/1/_bv8ZXU5im0r88bg6KA8Ma03lMP-UzUK.jpg",
      41.4976997,
      70.0581969,
      "Ugam-Chotqol milliy parki hududi o‘zining shaffof ko‘llari va so‘lim daraxtlari bilan ajoyib tabiiy manzara kashf qiladi. Urungach nefrit ko‘llari milliy parkning gavhari hisoblanadi. Urungachning quyi va yuqori ko‘llari bo‘ylab sayohat qilar ekansiz, tiniq suvlarda suzish, to‘g‘ongacha sayr qilish va yuqori ko‘l suvlarida aks etuvchi qoyalar bilan tanishish mumkin. Shahargacha bo‘lgan masofa: 150 kilometr"),
  Sea(
      "Sangardak sharsharasi",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSMtg2EWRSahA5fV6EJt_4INO8BLO5nSSKOhg&usqp=CAU",
      41.4976997,
      70.0581969,
      "Sangardak sharsharasi – Surxondaryo viloyatining Sariosiyo tumani markazidan qariyb 55 kilometr uzoqlikda, togʻlar orasidagi Sangardak qishlogʻida joylashgan.Sharshara yer sathidan 120 metr balandlikdagi togʻ qoyasidan otilib tushadi. Uning suvi yoz faslida koʻpayadi, kuzda esa kamaya boshlaydi. Mahalliy aholining aytishicha, uning zilol suvi baland togʻlardagi muzliklarning erishidan hosil boʻladi.Oʻziga xos ajib tabiati, mayin esuvchi salqin shabadasi bu joyni kishilarning sevimli ziyorat maskaniga aylantirgan. Sharsharaning zilol suvidan bahramand boʻlish, oftob deyarli tushmaydigan xushhavo manzarani tomosha qilish istagidagi sayyohlarning sanogʻi yoʻq."),
  Sea(
      "Chorvoq",
      "https://st2.depositphotos.com/2211028/7679/i/950/depositphotos_76792563-stock-photo-charvak-water-reservoir-the-nature.jpg",
      41.635144,
      69.940021,
      "O‘zbekistonda plyaj ta’tilini o‘ylab yurgan bo‘lsangiz Toshkent viloyatining Bo‘stonliq tumanida joylashgan Chorvoq suv omboriga e’tibor qaratishingiz kerak. Dam olish maskaniga poytaxtdan taksi, poyezd yoki avtobusda borish mumkin.Ulkan suv havzasi bo‘yidagi jihozlangan plyajlar dam olish joylari, soyabonlar, an’anaviy yog‘och karavotlar va kafelarni taklif etadi. Bu yerda dam oluvchilar skuterda, katamaranda yoki suv bananida uchishi mumkin, ekstremal sport turlari muxlislari paraplanda uchib, tevarak atrofni o‘rganishlari mumkin.Chorvoqning butun qirg‘oq bo‘yi bo‘ylab 100 km masofada dam olish maskanlari, bolalar lageri va xususiy uylar mavjud bo‘lib, ularni o‘zingiz ham, istalgan mahalliy sayyohlik agentliklari orqali ham band qilishingiz mumkin.Plyaj ta‘tilini siz bu yerda yangi kashfiyotlar bilan mujassamlashtirib – Bo‘stonliq tumanidagi eng qadimiy arxeologik obidalarni ko‘rish, qoyatosh rasmlari va ibtidoiy odamlarning manzilgohlarini ko‘rish, qo‘riqlanadigan hududning sirli daralari va g‘orlarida sayr qilishingiz mumkin."),
  Sea(
      "Aydarko'l",
      "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQTWrQn28_qAAF4U_zrCs0opCXc5vy4s6bYVA&usqp=CAU",
      41.4976997,
      70.0581969,
      "Suv sayyohligi yo‘nalishlari ro‘yxatida bepoyon Qizilqum cho‘lida joylashgan Aydarko‘l turg‘un ko‘li munosib o‘rin egallaydi.Suvning go‘zal rangi va taʼsirchan kattaligi tufayli, xushmanzara ko‘l ko‘pincha “qumlikdagi firuza dengiz” deb nomlanadi. Aydarko‘l bo‘yida ikki mingdan ortiq bo‘lmagan odam yashaganligi sababli, ko‘l atrofidagi tabiat yovvoyi va inson oyog‘i tegmagan bo‘lib qolmoqda.Ko‘lning qirg‘oqlari Navoiy viloyatining Nurota tumanidan Jizzax viloyatining Forish va Mirzacho‘l tumanlarigacha cho‘zilgan. Ko‘lda suv osti oqimlari yo‘q va undagi tuz miqdori ham unchalik yuqori emas, bu esa ushbu hududda nabotot va hayvonot olamining rivojlanishiga ijobiy taʼsir ko‘rsatadi. Baliq ovlash mavsumini rivojlantirish va muvaffaqiyatli ovlashda oddiy sazan, sudak, oqqayroq, laqqa baliq, chexon kabi ko‘plab baliq turlari mavjudligi qulaylik tug‘diradi.Ko‘lning tepasida tez-tez ko‘tarilib turadigan pushti pelikanlarni, nafis oqqushlarni ko‘rish mumkin. Bu yerda Qizil kitobga kiritilgan qushlar ham boshpana topgan – jingalak pelikan, kichkina baqlan, qizil bo‘yinli kazarka, kul rang o‘rdak-piskulka, oq ko‘zli botqaldoq, dasht bo‘ktargi, oq dumli suvburgut, qora kalxat.Ko‘lning nabotot va hayvonot olamining xilma-xilligidan tashqari, Aydarko‘l dam olish uchun eng yaxshi joylardan biri hisoblanadi. Sohillari ajablanarli darajada yumshoq va toza qum bilan qoplangan ko‘p kilometrlik plyajlar, qirg‘oq atrofida zich o‘sgan suvli o‘t-o‘lanlarning tozaligi, shuningdek, qirg‘oq zonasidagi tinchlik va osoyishtalik ko‘plab ijobiy taassurotlar va unutilmas daqiqalarni his qilishda yordam beradi."),
  Sea(
      "Tuzkon",
      "https://s3.nat-geo.ru/images/2019/5/16/ccce87586f3747eea76c1b0b6c266148.max-1200x800.jpg",
      41.4976997,
      70.0581969,
      "Bu ko'l Jizzax viloyatida, Jizzaxdan 56 kilometr shimoli-sharqda, Qizilqum cho'lining sharqiy qismida joylashgan. Taxminlarga ko'ra, Tuzkon ko'li - Orol - Kaspiy davridagi suv havzasining bir qismidir. Ko'l suvi xech qayerga oqmaydi, shuningdek, u bir oz sho'rroq bo'lib, shimoli-g'arbda Aydarko'l bilan bog'lanadi. Bu ko'llarning umuiy maydoni - taxminan 3750 kilometr kvadrat. Bir yilda ko'llar tizimida 2000 tonnagacha baliq ovlanadi. "),
  Sea(
      "Sudochye",
      "https://www.people-travels.com/images/uzbekistan-rivers/lake-sudochye.jpg",
      41.4976997,
      70.0581969,
      "SudochyeyokiSudovshinko’li – O’zbekistondagiko’l, Qoaraqalpog’istonRespublikasida.Sudochye ko’li Amudaryo deltasining g’arbiy qismida joylashgan. Maydoni – 333 km² (2006 yil holatiga ko’ra), dengiz sathidan 53 m balandlikda. Uni Amudaryoning Ravshan va Priemuzyak qo’ltiqlari oziqlantiradi. Orol dengizinig qurishidan oldin u bilan tor kanal asosida ulangan va baliq turlariga yumurtlama vazifasini o’tagan.Sudochyeda yiliga baliq ulovi 2000 t yetgan, maksimal chuqurligi 3 m tashkil qilgan.Amudaryodan suvning intensiv tarzda qabul qilishi natijasida uning deltasinig muhim darajada qurishiga olib keldi va yaqinda ko’l o’zining qishloq xo’jalik ahamiyatini yo’qotdi. Hozirgi vaqtda asosan kollektor-drenaj suvlari hisobiga oqim qayta ta’mirlandi. Sudochye ko’li ba’zan to’liq to’lgan va hattoki alohida davrlarda suv qismlarini G’arbiy Orolga uzatish rejalashtirilgan.Sudochye ko’lining minerallashuvi 3-4%ni tashkil qiladi. Yilning issiq vaqtida 25-27°С gacha isiydi. Noyabr-dekabrda ko’l muzlaydi, fevral oxirida – aprel boshida ochiladi.Yaqin shaharlar: Qo’ng’irot, Chimboy, Nukus")
];

class Category {
  final Color backColor;
  final Color textColor;
  final String name;
  final String img;
  final List list;

  Category(this.backColor, this.textColor, this.name, this.img, this.list);
}

List<Category> categories = [
  Category(Color(0xffFFe8ec), Colors.pink, 'Sanatoriyalar',
      "assets/svg/beach.svg", beaches),
  Category(Color(0xffd7f7d8), Colors.green, 'Soylar', "assets/svg/river.svg",
      rivers),
  Category(Color(0xffFFEFE5), Colors.deepOrange, 'Tog\'lar',
      "assets/svg/mountainn.svg", mountains),
  Category(
      Color(0xffE2F0F9), Colors.blue, 'Ko\'llar', "assets/svg/waves.svg", seas),
];
