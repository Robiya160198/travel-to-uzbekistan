import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'models/recommended.dart';

class Test extends StatefulWidget {
  final rec;
  Test(this.rec);
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  GoogleMapController googleMapController;
  final Map<String, Marker> _markers = {};

  Map<PolylineId, Polyline> polylines = {};
  void _getLocation() async {
    var currentLocation = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    double distanceInMeters = await Geolocator().distanceBetween(
        currentLocation.latitude,
        currentLocation.longitude,
        widget.rec.latitude,
        widget.rec.longitude);
    List<Placemark> start = await Geolocator().placemarkFromAddress("Bukhara");
    setState(() {
      _markers.clear();
      final marker = Marker(
        markerId: MarkerId("curr_loc"),
        position: LatLng(widget.rec.latitude, widget.rec.longitude),
        infoWindow: InfoWindow(
            title: '${widget.rec.name}   ${distanceInMeters / 1000} km'),
      );
      _markers["Current Location"] = marker;

      //    _createPolylines(currentLocation, Position());
    });
  }

  @override
  void initState() {
    _getLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${widget.rec.name}',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: GoogleMap(
        onMapCreated: onMapCreated,
        myLocationEnabled: true,
        initialCameraPosition: CameraPosition(
            target: LatLng(widget.rec.latitude, widget.rec.longitude),
            zoom: 14),
        mapType: MapType.normal,
        markers: _markers.values.toSet(),
      ),
    );
  }

  void onMapCreated(GoogleMapController controller) {
    setState(() {
      controller = googleMapController;
    });
  }
}
